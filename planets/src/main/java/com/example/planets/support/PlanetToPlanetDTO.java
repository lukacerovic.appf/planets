package com.example.planets.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.planets.dto.PlanetDTO;
import com.example.planets.model.Planet;

@Component
public class PlanetToPlanetDTO implements Converter<Planet,PlanetDTO> {

	@Override
	public PlanetDTO convert(Planet planet) {
		
		PlanetDTO dto = new PlanetDTO();
		dto.setId(planet.getId());
		dto.setName(planet.getName());
		dto.setSurfaceArea(planet.getSurfaceArea());
		dto.setMass(planet.getMass());
		dto.setDistanceFromSun(planet.getDistanceFromSun());
		dto.setAverageSurfaceTemperature(planet.getAverageSurfaceTemperature());
		
		
		return dto;
		
		
	}
	
	public List<PlanetDTO> convert(List<Planet> planets){
		List<PlanetDTO> planetsDto = new ArrayList<>();

		for(Planet p : planets) {
			PlanetDTO dto = convert(p);
			planetsDto.add(dto);
		}

		return planetsDto;
	}
}
