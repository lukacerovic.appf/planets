package com.example.planets.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.planets.dto.SatellitePlanetDTO;
import com.example.planets.model.Satellite;

@Component
public class SatellitePlanetToSatellitePlanetDTO implements Converter<Satellite,SatellitePlanetDTO>{

	@Override
	public SatellitePlanetDTO convert(Satellite satellite) {
		SatellitePlanetDTO dto = new SatellitePlanetDTO();
		dto.setPlanetId(satellite.getPlanet().getId());
		dto.setPlanetName(satellite.getPlanet().getName());
		dto.setPlanetSurfaceArea(satellite.getPlanet().getSurfaceArea());
		dto.setPlanetMass(satellite.getPlanet().getMass());
		dto.setPlanetDistanceFromSun(satellite.getPlanet().getDistanceFromSun());
		dto.setPlanetAverageSurfaceTemperature(satellite.getPlanet().getAverageSurfaceTemperature());
		dto.setId(satellite.getId());
		dto.setName(satellite.getName());
		dto.setSurfaceArea(satellite.getSurfaceArea());
		dto.setMass(satellite.getMass());
		dto.setNaturalSatellite(satellite.getNaturalSatellite());
		
		return dto;
	}
	
	public List<SatellitePlanetDTO> convert(List<Satellite> satellites){
        List<SatellitePlanetDTO> satellitesPlanetDto = new ArrayList<>();

        for(Satellite s : satellites) {
            SatellitePlanetDTO dto = convert(s);
            satellitesPlanetDto.add(dto);
        }

        return satellitesPlanetDto;
    }
	

}
