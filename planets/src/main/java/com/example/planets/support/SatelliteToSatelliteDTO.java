package com.example.planets.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.planets.dto.SatelliteDTO;
import com.example.planets.model.Satellite;

@Component
public class SatelliteToSatelliteDTO implements Converter<Satellite,SatelliteDTO>{

	@Override
	public SatelliteDTO convert(Satellite satellite) {
		
		SatelliteDTO dto = new SatelliteDTO();
		dto.setId(satellite.getId());
		dto.setName(satellite.getName());
		dto.setSurfaceArea(satellite.getSurfaceArea());
		dto.setMass(satellite.getMass());
		dto.setNaturalSatellite(satellite.getNaturalSatellite());
		dto.setPlanetId(satellite.getPlanet().getId());
		dto.setPlanetName(satellite.getPlanet().getName());
		
		
		return dto;
	}
	
	public List<SatelliteDTO> convert(List<Satellite> satellites){
		List<SatelliteDTO> satellitesDto = new ArrayList<>();

		for(Satellite s : satellites) {
			SatelliteDTO dto = convert(s);
			satellitesDto.add(dto);
		}

		return satellitesDto;
	}

}
