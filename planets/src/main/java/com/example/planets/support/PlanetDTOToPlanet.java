package com.example.planets.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.planets.dto.PlanetDTO;
import com.example.planets.model.Planet;

@Component
public class PlanetDTOToPlanet implements Converter<PlanetDTO,Planet> {

	@Override
	public Planet convert(PlanetDTO dto) {
		
		Planet planet = new Planet();
		planet.setId(dto.getId());
		planet.setName(dto.getName());
		planet.setSurfaceArea(dto.getSurfaceArea());
		planet.setMass(dto.getMass());
		planet.setDistanceFromSun(dto.getDistanceFromSun());
		planet.setAverageSurfaceTemperature(dto.getAverageSurfaceTemperature());
		
		return planet;
	}

}
