package com.example.planets.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.planets.dto.SatellitePlanetDTO;
import com.example.planets.model.Planet;
import com.example.planets.model.Satellite;
import com.example.planets.service.PlanetService;


@Component
public class SatellitePlanetDTOToSatellitePlanet implements Converter<SatellitePlanetDTO,Satellite>{

	@Autowired 
	private PlanetService planetService;
	
	@Override
	public Satellite convert(SatellitePlanetDTO dto) {
		Satellite satellite = new Satellite();
		satellite.setId(dto.getId());
		satellite.setName(dto.getName());
		satellite.setSurfaceArea(dto.getSurfaceArea());
		satellite.setMass(dto.getMass());
		satellite.setNaturalSatellite(dto.getNaturalSatellite());
		
		Planet planet = planetService.findOne(dto.getPlanetId()).get();
		satellite.setPlanet(planet);
		
		
		return satellite;
	}

}
