package com.example.planets.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.planets.model.Satellite;
import com.example.planets.repository.SatelliteRepository;
import com.example.planets.service.SatelliteService;

@Service
public class SatelliteServiceIMPL implements SatelliteService {
	
	@Autowired
	private SatelliteRepository satelliteRepository;

	@Override
	public Satellite save(Satellite satellite) {
		// TODO Auto-generated method stub
		return satelliteRepository.save(satellite);
	}

	@Override
	public Optional<Satellite> findOne(Long id) {
		// TODO Auto-generated method stub
		return satelliteRepository.findById(id);
	}

	@Override
	public Satellite update(Satellite satellite) {
		// TODO Auto-generated method stub
		return satelliteRepository.save(satellite);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		satelliteRepository.deleteById(id);
		
	}

	@Override
	public List<Satellite> findAllByPlanetId(Long id) {
		// TODO Auto-generated method stub
		return satelliteRepository.findAllByPlanet_id(id);
	}

}
