package com.example.planets.service;


import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.example.planets.model.Planet;


public interface PlanetService {

	Planet save(Planet planet);
	
	Optional<Planet> findOne(Long id); 
	
	Page<Planet> filter(String planetName, int page); 
	
	Planet update (Planet planet); 
	
	void delete(Long id); 
	
	List<Planet> findAll();
	
	Page<Planet> findAllByPage(int page);
	
	
	
	
}
