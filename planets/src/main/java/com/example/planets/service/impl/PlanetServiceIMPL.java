package com.example.planets.service.impl;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.planets.model.Planet;
import com.example.planets.repository.PlanetRepository;
import com.example.planets.service.PlanetService;

@Service
public class PlanetServiceIMPL implements PlanetService {

	@Autowired
	private PlanetRepository planetRepository;
	
	@Override
	public Planet save(Planet planet) {
		// TODO Auto-generated method stub
		return planetRepository.save(planet);
	}

	@Override
	public Page<Planet> filter(String planetName, int page) {
		// TODO Auto-generated method stub
		if (planetName != null) {
			planetName = "%" + planetName + "%";
		}
		
		return planetRepository.filter(planetName, PageRequest.of(page, 3));
	}


	@Override
	public Planet update(Planet planet) {
		// TODO Auto-generated method stub
		return planetRepository.save(planet);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		planetRepository.deleteById(id);
	}

	@Override
	public Optional<Planet> findOne(Long id) {
		// TODO Auto-generated method stub
		return planetRepository.findById(id);
	}

	@Override
	public Page<Planet> findAllByPage(int page) {
		// TODO Auto-generated method stub
		return planetRepository.findAll(PageRequest.of(page, 3));
	}

	@Override
	public List<Planet> findAll() {
		// TODO Auto-generated method stub
		return planetRepository.findAll();
	}



}
