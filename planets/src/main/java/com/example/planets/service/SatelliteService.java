package com.example.planets.service;

import java.util.List;
import java.util.Optional;

import com.example.planets.model.Satellite;


public interface SatelliteService {
	
	Satellite save(Satellite satellite);
	
	List<Satellite> findAllByPlanetId(Long id);
	
	Optional<Satellite> findOne(Long id);
	
	Satellite update(Satellite satellite);
	
	void delete(Long id);
	
	
}
