package com.example.planets.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.planets.dto.PlanetDTO;
import com.example.planets.dto.SatellitePlanetDTO;
import com.example.planets.model.Planet;
import com.example.planets.model.PlanetsCount;
import com.example.planets.model.Satellite;
import com.example.planets.service.PlanetService;
import com.example.planets.service.SatelliteService;
import com.example.planets.support.PlanetDTOToPlanet;
import com.example.planets.support.PlanetToPlanetDTO;
import com.example.planets.support.SatellitePlanetToSatellitePlanetDTO;



@RestController
@RequestMapping("/planet")
public class PlanetController {
	
	@Autowired
	private PlanetService planetService;
	
	@Autowired
	private PlanetToPlanetDTO toDTO;
	
	@Autowired
	private PlanetDTOToPlanet toEntity;
	
	@Autowired
	private SatelliteService satelliteService;
	
	@Autowired
	private SatellitePlanetToSatellitePlanetDTO satellitePlanetToSatellitePlanetDTO;
	

	
	@GetMapping("/filter")
    private ResponseEntity<List<PlanetDTO>> getAll(@RequestParam(value = "planetName", required = false)
                                                               String planetName,
                                                   @RequestParam(value = "page",
                                                           defaultValue = "0") int page) {

        Page<Planet> pageNo = null;

        if(planetName != null) {
            pageNo = planetService.filter(planetName, page);
        } else {
            pageNo = planetService.findAllByPage(page);
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Total-Pages", Integer.toString(pageNo.getTotalPages()));

        return new ResponseEntity<>(toDTO.convert(pageNo.getContent()), headers, HttpStatus.OK);
    }

	
	@PutMapping("/{id}")
	public ResponseEntity<PlanetDTO> update(@PathVariable Long id,@Validated @RequestBody PlanetDTO dto){
		if(!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Planet res = toEntity.convert(dto); 
		Planet planet = planetService.save(res);
		
		return new ResponseEntity<>(toDTO.convert(planet), HttpStatus.OK);
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<HttpStatus> delete(@PathVariable Long id){
		System.out.println(id);
		planetService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
		
	}
	
	@GetMapping("/viewAllSatellitesOfOnePlanet/{id}")
    public ResponseEntity<List<SatellitePlanetDTO>> getAllSatellitesByPlanetId(@PathVariable Long id) {

        List<Satellite> satellites = satelliteService.findAllByPlanetId(id);

        if(satellites == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(satellitePlanetToSatellitePlanetDTO.convert(satellites), HttpStatus.OK);
    }
	
	@PostMapping("/create")
    public ResponseEntity<PlanetDTO> add(@RequestBody PlanetDTO newDto) {

        Planet planet = toEntity.convert(newDto);
        Planet saved = planetService.save(planet);

        return new ResponseEntity<>(toDTO.convert(saved), HttpStatus.CREATED);
    }
	
	@GetMapping("/sortedBySatellite")
	
	public ResponseEntity<List<PlanetDTO>> getAll(@RequestParam Integer page) {
		try {
		List<Planet> planets = planetService.findAll();
		ArrayList<PlanetsCount> planetsCount = new ArrayList<PlanetsCount>();
		for (Planet planet: planets) {
			List<Satellite> satellites = satelliteService.findAllByPlanetId(planet.getId());
			planetsCount.add(new PlanetsCount(planet,satellites.size()));
		}
		Collections.sort(planetsCount, Comparator.comparing(PlanetsCount::getCount));
		Collections.reverse(planetsCount);
		List<Planet> sortedPlanets = new ArrayList<Planet>();
		for (PlanetsCount planetCount: planetsCount) {
			
			sortedPlanets.add(planetCount.planet);
		}
	
	
		Integer validPage = page-1;
	    Long start =   PageRequest.of(validPage, 3).getOffset();
	    Long end = (start +  PageRequest.of(validPage, 3).getPageSize()) > sortedPlanets.size() ? sortedPlanets.size() : (start +  PageRequest.of(validPage, 3).getPageSize());
	
	   
	   
	    Page<Planet> planetPage= new PageImpl<Planet>(sortedPlanets.subList(start.intValue(), end.intValue()),  PageRequest.of(validPage, 3), sortedPlanets.size());
	   
	
	    return new ResponseEntity<>(toDTO.convert(planetPage.getContent()), HttpStatus.OK); 
		} catch (Exception e){
			System.out.println("page doesn't exist");
		}
		List<PlanetDTO> emptyList = new ArrayList<>();
		return new ResponseEntity<>(emptyList, HttpStatus.OK);
	   
	  
		
	}
	


}

