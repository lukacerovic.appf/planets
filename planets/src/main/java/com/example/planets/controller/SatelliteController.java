package com.example.planets.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.planets.dto.SatelliteDTO;
import com.example.planets.model.Satellite;
import com.example.planets.service.SatelliteService;
import com.example.planets.support.SatelliteDTOToSatellite;
import com.example.planets.support.SatelliteToSatelliteDTO;

@RestController
@RequestMapping("/satellite")
public class SatelliteController {
	
	@Autowired
	private SatelliteService satelliteService;
	
	@Autowired
	private SatelliteToSatelliteDTO toDTO;
	
	@Autowired
	private SatelliteDTOToSatellite toEntity;
	
	
	
	@GetMapping("/{id}")
	public ResponseEntity<SatelliteDTO> getOne(@PathVariable Long id){
		Optional<Satellite> satellite = satelliteService.findOne(id);
		if(!satellite.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(toDTO.convert(satellite.get()), HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<SatelliteDTO> update(@PathVariable Long id,@Validated @RequestBody SatelliteDTO dto) {
		
		if(!id.equals(dto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		Satellite res = toEntity.convert(dto);
		Satellite satellite = satelliteService.save(res);
		
		return new ResponseEntity<>(toDTO.convert(satellite), HttpStatus.OK);
		
	}
	
	@DeleteMapping("/{id}") 
	public ResponseEntity<HttpStatus> delete(@PathVariable Long id){
		System.out.println(id);
		satelliteService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
		
	}
	
	@PostMapping("/create")
    public ResponseEntity<SatelliteDTO> add(@RequestBody SatelliteDTO newDto) {

        Satellite satellite = toEntity.convert(newDto);
        Satellite saved = satelliteService.save(satellite);
        
        if (newDto.getId() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(toDTO.convert(saved), HttpStatus.CREATED);
    }

    @GetMapping("/viewAllSatellitesOfOnePlanet/{id}")
    public ResponseEntity<List<SatelliteDTO>> getAllByPlanetId(@PathVariable Long id) {

        List<Satellite> satellites = satelliteService.findAllByPlanetId(id);

        if(satellites == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(toDTO.convert(satellites), HttpStatus.OK);
    }

}
