package com.example.planets.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.planets.model.Satellite;

@Repository
public interface SatelliteRepository extends JpaRepository <Satellite, Long> {

	List<Satellite> findAllByPlanet_id(Long id);
}
