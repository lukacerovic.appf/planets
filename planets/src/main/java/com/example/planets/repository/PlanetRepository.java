package com.example.planets.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.example.planets.model.Planet;

@Repository
public interface PlanetRepository extends JpaRepository<Planet,Long> {
	
	@Query("SELECT p FROM Planet p WHERE" +
            "(:planetName = NULL OR p.name LIKE %:planetName%)")
	Page<Planet> filter(@Param("planetName")String planetName,Pageable pageable);
	

}
