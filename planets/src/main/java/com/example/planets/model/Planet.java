package com.example.planets.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

//Planet object fields:
//- name (String) (mandatory)
//- surfaceArea (Long) (mandatory)
//- mass (Long) (mandatory)
//- distanceFromSun (Long) (mandatory)
//- averageSurfaceTemperature (Integer) (optional)
//- satellites (List<Satellite>)

@Entity
public class Planet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(nullable=false)
	String name;
	
	@Column(nullable=false)
	Long surfaceArea;
	
	@Column(nullable=false)
	Long mass;
	
	@Column(nullable=false)
	Long distanceFromSun;
	
	@Column 
	Integer averageSurfaceTemperature;
	
	@OneToMany(mappedBy = "planet",cascade=CascadeType.ALL)
	List<Satellite> satellites = new ArrayList<Satellite>();

	
	public Planet(Long id, String name, Long surfaceArea, Long mass, Long distanceFromSun,
			Integer averageSurfaceTemperature) {
		super();
		this.id = id;
		this.name = name;
		this.surfaceArea = surfaceArea;
		this.mass = mass;
		this.distanceFromSun = distanceFromSun;
		this.averageSurfaceTemperature = averageSurfaceTemperature;
		
	}


	public Planet(String name, Long surfaceArea, Long mass, Long distanceFromSun, Integer averageSurfaceTemperature,
			List<Satellite> satellites) {
		super();
		this.name = name;
		this.surfaceArea = surfaceArea;
		this.mass = mass;
		this.distanceFromSun = distanceFromSun;
		this.averageSurfaceTemperature = averageSurfaceTemperature;
		this.satellites = satellites;
	}
	
	public Planet() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Long getSurfaceArea() {
		return surfaceArea;
	}


	public void setSurfaceArea(Long surfaceArea) {
		this.surfaceArea = surfaceArea;
	}


	public Long getMass() {
		return mass;
	}


	public void setMass(Long mass) {
		this.mass = mass;
	}


	public Long getDistanceFromSun() {
		return distanceFromSun;
	}


	public void setDistanceFromSun(Long distanceFromSun) {
		this.distanceFromSun = distanceFromSun;
	}


	public Integer getAverageSurfaceTemperature() {
		return averageSurfaceTemperature;
	}


	public void setAverageSurfaceTemperature(Integer averageSurfaceTemperature) {
		this.averageSurfaceTemperature = averageSurfaceTemperature;
	}


	public List<Satellite> getSatellites() {
		return satellites;
	}


	public void setSatellits(List<Satellite> satellites) {
		this.satellites = satellites;
	}
	
	
	
	
	
	
	

}
