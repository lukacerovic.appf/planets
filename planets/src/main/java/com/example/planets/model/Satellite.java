package com.example.planets.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

//Satellite object fields:
//- name (String) (mandatory)
//- surfaceArea (Long) (mandatory)
//- mass (Long) (mandatory)
//- naturalSatelitte (Boolean) (optional)

@Entity
public class Satellite {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	
	@Column(nullable=false)
	String name;
	
	@Column(nullable=false)
	Long surfaceArea;
	
	@Column(nullable=false)
	Long mass;
	
	@Column 
	Boolean naturalSatellite;
	
	@ManyToOne
	@JoinColumn(name="planet_id",nullable=false)
	Planet planet;

	public Satellite(Long id, String name, Long surfaceArea, Long mass, Boolean naturalSatellite, Planet planet) {
		super();
		this.id = id;
		this.name = name;
		this.surfaceArea = surfaceArea;
		this.mass = mass;
		this.naturalSatellite = naturalSatellite;
		this.planet = planet;
	}

	public Satellite(String name, Long surfaceArea, Long mass, Boolean naturalSatellite, Planet planet) {
		super();
		this.name = name;
		this.surfaceArea = surfaceArea;
		this.mass = mass;
		this.naturalSatellite = naturalSatellite;
		this.planet = planet;
	}
	
	public Satellite() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSurfaceArea() {
		return surfaceArea;
	}

	public void setSurfaceArea(Long surfaceArea) {
		this.surfaceArea = surfaceArea;
	}

	public Long getMass() {
		return mass;
	}

	public void setMass(Long mass) {
		this.mass = mass;
	}

	public Boolean getNaturalSatellite() {
		return naturalSatellite;
	}

	public void setNaturalSatellite(Boolean naturalSatellite) {
		this.naturalSatellite = naturalSatellite;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}
	
	

}
