package com.example.planets.model;

public class PlanetsCount {
	
	public Planet planet;
	public Integer count;
	
	
	public PlanetsCount(Planet planet, Integer count) {
		super();
		this.planet = planet;
		this.count = count;
	}
	
	public Integer getCount() {
		return count;
	}
	
	
	
}