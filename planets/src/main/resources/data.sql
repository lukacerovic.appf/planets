

INSERT INTO planet (id, name, surface_area, mass, distance_from_sun, average_surface_temperature)
VALUES ('100', 'Mars', 114, 642, 1520, -65);
INSERT INTO planet (id, name, surface_area, mass, distance_from_sun, average_surface_temperature)
VALUES ('200', 'Venus', 460, 487, 720, 464);
INSERT INTO planet (id, name, surface_area, mass, distance_from_sun, average_surface_temperature)
VALUES ('300', 'Saturn', 4200, 597, 9540, -140);
INSERT INTO planet (id, name, surface_area, mass, distance_from_sun, average_surface_temperature)
VALUES ('400', 'Jupiter', 6100, 190, 5200, -110);
INSERT INTO planet (id, name, surface_area, mass, distance_from_sun, average_surface_temperature)
VALUES ('500', 'Neptun', 7000, 102, 30000, -200);

INSERT INTO satellite (id, name, surface_area, mass, natural_satellite, planet_id)
VALUES ('100', 'Moon', 25, 50, true, 100);
INSERT INTO satellite (id, name, surface_area, mass, natural_satellite, planet_id)
VALUES ('200', 'Triton', 20, 100, true, 500);
INSERT INTO satellite (id, name, surface_area, mass, natural_satellite, planet_id)
VALUES ('300', 'Titan', 18, 90, true, 100);
INSERT INTO satellite (id, name, surface_area, mass, natural_satellite, planet_id)
VALUES ('400', 'Sputnik', 2, 11, false, 500);
INSERT INTO satellite (id, name, surface_area, mass, natural_satellite, planet_id)
VALUES ('500', 'Explorer', 2, 7, false, 500);