package com.example.planets.junit;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.example.planets.model.Planet;
import com.example.planets.model.Satellite;
import com.example.planets.repository.PlanetRepository;
import com.example.planets.repository.SatelliteRepository;

@DataJpaTest
public class TestJunit {

    @Autowired
    private SatelliteRepository satelliteRepository;

    @Autowired
    private PlanetRepository planetRepository;

    @Test
    @Order(1)
    public void getSatelliteTest(){

        Satellite satellite = satelliteRepository.findById(100L).get();
        System.out.println(satellite.getName());

        Assertions.assertThat(satellite.getId()).isEqualTo(100L);

    }

    @Test
    @Order(2)
    @Rollback(value = false)
    public void savePlanetTest(){

        Planet planet = new Planet(20L, "Severus", 32L, 33L, 23L, 2);

        planetRepository.save(planet);

        System.out.println(planet.getName());

        Assertions.assertThat(planet.getId()).isGreaterThan(0);
    }

    @Test
    @Order(3)
    @Rollback(value = false)
    public void getAllPlanets() {

        List<Planet> planets = planetRepository.findAll();
        System.out.println(planets.get(0).getName());

        Assertions.assertThat(planets);
    }
}