package com.example.planets.e2eintegrationtest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import com.example.planets.model.Planet;

import com.example.planets.repository.PlanetRepository;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {

	@LocalServerPort
	private int port;
	
	private String baseUrl = "http://localhost";
	
	private static RestTemplate restTemplate;
	
	private Planet planet1;
	private Planet planet2;

	
	@Autowired
	PlanetRepository planetRepository;
	
	
	@BeforeAll
	public static void init() {
		restTemplate = new RestTemplate();
	}
	
	@BeforeEach
	public void beforeSetup() {
		
//		baseUrl = baseUrl + ":" +port + "/planet";
		baseUrl = baseUrl + ":" +port + "/api/planet";
		
		planet1 = new Planet();
		planet1.setId(1l);
		planet1.setName("TestPlanet1");
		planet1.setSurfaceArea(11L);
		planet1.setMass(12L);
		planet1.setDistanceFromSun(13L);
		planet1.setAverageSurfaceTemperature(15);
		
		planet2 = new Planet();
		planet2.setId(2l);
		planet2.setName("TestPlanet2");
		planet2.setSurfaceArea(21L);
		planet2.setMass(22L);
		planet2.setDistanceFromSun(23L);
		planet2.setAverageSurfaceTemperature(25);
		
		
		
		planet1=planetRepository.save(planet1);
		planet1=planetRepository.save(planet2);
		

	}
	
	@AfterEach
	public void afterSetup() {
		planetRepository.deleteAll();
	}
	
	@Test
	void shouldCreatePlanetTest() {
		Planet planet1 = new Planet();
		planet1.setId(1l);
		planet1.setName("TestPlanet1");
		planet1.setSurfaceArea(11L);
		planet1.setMass(12L);
		planet1.setDistanceFromSun(13L);
		planet1.setAverageSurfaceTemperature(15);
				
		
		Planet newPlanet = restTemplate.postForObject(baseUrl+"/create", planet1, Planet.class);
		
		assertNotNull(newPlanet);
		assertThat(newPlanet.getId()).isNotNull();
	}
	
	@Test
	void shouldDeletePlanetTest() {
		
		restTemplate.delete(baseUrl+"/"+planet1.getId());
		
		int count = planetRepository.findAll().size();
		
		assertEquals(1, count);
	}

	
}

